﻿define([
 'jquery',
 'underscore',
 'backbone',
 'backbone.validation',
 'vent',
 'req',
 'marionette',
 'app/router',
 'app/templateHelpers',
 'moment'
], function ($,_, Backbone, BackboneValidation, Vent, Req, Marionette, AppRouter, TemplateHelpers, moment) {

    var router = new AppRouter();
    var App = new Marionette.Application();
   
    _.extend(App, {

        showError: function(errorMessage) {
            $('#error').show();
        },

        hideError: function(){
            $('#error').hide();
        },

        navigate: function (fragment, options) {
            App.hideError();
            options = _.extend({ trigger: false }, options);
            Backbone.history.navigate(fragment, options);
        }
    });

    App.addInitializer(function () {

        Req.setHandler('confirmDelete', function (type) {
            var msg = 'Are you sure you want to delete this ' + (type || 'item') + '?';
            return confirm(msg);
        });

        Vent.on('error', App.showError);

        // if the user navigates then hide the error
        Backbone.history.on('all', App.hideError);
    });

    // setup regions
    App.addRegions({
        nav: '#nav',
        content: '#content'
    });

    // Override the ajax calls to add versioning to the url
    Backbone.ajax = function () {
        var args = Array.prototype.slice.call(arguments, 0);
        var ajaxArgs = args[0];

        if (/^[/]*api/.test(ajaxArgs.url) == false) {
            ajaxArgs.url = '/v1' + ajaxArgs.url;
        }

        var errorCallback = $.noop();

        if (typeof(ajaxArgs.error) === 'function') {
            errorCallback = ajaxArgs.error;
        }

        ajaxArgs.error = function (xhr, status, error) {
            Vent.trigger('error:ajax', error);
            Vent.trigger('error', error);
            errorCallback.call(this, xhr, status, error);
        };

        return Backbone.$.ajax.apply(Backbone.$, args);
    }

    _.extend(Backbone.Model.prototype, BackboneValidation.mixin);

    // Make sure the template helpers get added to all templates
    Marionette.Renderer.render = function(template, data) {
        data = _.extend({}, data, TemplateHelpers);

        if (typeof template === 'function') {
            return template(data);
        }

        return '';
    }

    return App;

});