﻿define([
    'underscore',
    'marionette'
], function (_, Marionette) {

    var ModalView = Marionette.ItemView.extend({

        events: {
            'click .button.close': 'hideModal'
        },

        className: 'modal-dialog',

        template: _.template('<div class="modal-content-wrapper"><div class="header"><div class="button close" role="button">&nbsp;</div><div class="title"><%= title %></div></div><div class="modal-content">&nbsp;</div><div class="clear">&nbsp;</div></div>'),

        serializeData: function () {
            return this.options;
        },

        setContent: function (viewContent) {
            this.$('.modal-content').html(viewContent);
        },

        hideModal: function () {
            this.trigger('hide:modal');
        }
        
    });
    
    var ModalRegion = Marionette.Region.extend({

        constructor: function () {
            _.bindAll(this, 'show', 'hideModal');
            Marionette.Region.prototype.constructor.apply(this, arguments);
        },

        show: function (view) {

            this.ensureEl();

            var dialogOptions = _.extend({
                title: 'New Dialog'
            }, view.dialog || {});
            
            this.modalView = new ModalView(dialogOptions);

            this.$el.html('');
            this.$el.append('<div class="modal-overlay" style="z-index:500">&nbsp;</div>');
            this.$el.append(this.modalView.render().el);
            this.modalView.setContent(view.render().el);

            view.$el.css('zIndex', 501);

            // listen for view events
            this.modalView.on('hide:modal', this.hideModal, this);
            view.on('dialog:close', this.hideModal, this);

            view.isInDialog = true;

            Marionette.triggerMethod.call(this, "show", view);
            Marionette.triggerMethod.call(view, "show");

            // show the modal
            this.$el.show();
            this.currentView = view;
            this.isOpen = true;
           
        },

        hideModal: function () {
            this.currentView.isInDialog = false;
            this.$el.html('').hide();
            this.isOpen = false;
        }

    });

    return ModalRegion;

})