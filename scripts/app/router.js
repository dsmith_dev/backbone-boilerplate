﻿define([
  'vent',
  'marionette'
], function (Vent, Marionette) {

    var AppRouter = Marionette.AppRouter.extend({
        routes: {
            '*actions': 'default'
        },

        'default': function () {
            Vent.trigger('default:view');
        }
    });

    return AppRouter;
});