﻿define(['underscore', 'Handlebars', 'moment'], function (_, Handlebars, moment) {
    // wire up global template helpers
    var templateHelpers = {
        // format a date
        formatDate: function (date, format) {
            if (!date) return;
            format = format || "M/D/YYYY";
            return moment(date).format(format);
        },

        formatDateTime: function (date, format) {
            if (!date) return;
            format = format || "M/D/YYYY h:mm A";
            return moment(date).format(format);
        },

        // format a date as time ago
        timeago: function (date) {
            if (!date) return;
            return moment(date).fromNow();
        },

        // joins an array
        join: function (array, separator, func) {
            if (!array) return;
            var a = array;
            if (typeof func === 'function') {
                a = _(array).map(func);
            }

            separator = separator || ', ';
            return a.join(separator);
        },

        iif: function (predicate, trueVal, falseVal) {
            return (predicate) ? trueVal : falseVal;
        },

        left: function (value, limit) {
            var str = "";

            if (_.isArray(value)) {
                for (var i = 0; i < value.length; i++) {
                    if (str.length >= limit) break;
                    str = str + value[i];
                }
            } else if (value) {
                str = value;
            }
            
            if (str.length > limit) {
                return str.substring(0, limit) + '...';
            } 
            
            return str;
        },

        formatAddress: function (address, city, state, zip) {
            var str = address;

            if (city || state || zip) {
                str = str + ',';

                if (city) str = str + ' ' + city;
                if (state) str = str + ' ' + state;
                if (zip) str = str + ' ' + zip;
            }

            return str;
        },

        formatNA: function (value) {
            if (!value) return 'N/A';
            
            return value;
        }

    }



    //
    // Handlebar templating helper registration
    //
    Handlebars.registerHelper('formatDate', function (date, options) {
        if (!date) return options.hash['default'] || '';
        return moment(date).format(options.hash['format'] || "M/D/YYYY");
    });

    Handlebars.registerHelper('formatDateTime', function (dateTime, options) {
        if (!dateTime) return options.hash['default'] || '';
        return moment(dateTime).format(options.hash['format'] || "M/D/YYYY h:mm A");
    });

    Handlebars.registerHelper('formatMoney', function (n, c) {
        // this is borrowed from a stackoverflow.com thread
        // http://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-money-in-javascript
        var c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    });

    Handlebars.registerHelper('formatAddress', function (address, city, state, zip, options) {
        var str = address;

        if (city || state || zip) {

            if (options.hash['breakAfterAddress']) {
                str = str + '<br />';
            } else {
                str = str + ',';
            }

            if (city) str = str + ' ' + city;
            if (state) str = str + ', ' + state;
            if (zip) str = str + ' ' + zip;
        }

        if (str == null) {
            return '';
        }

        return new Handlebars.SafeString(str);
    });

    Handlebars.registerHelper('formatNA', function (val, options) {
        if (!val) return options.hash['na'] || 'N/A';
        return val;
    });

    Handlebars.registerHelper('timeago', function (date, options) {
        if (!date) return options.hash['default'] || '';
        return moment(date).fromNow();
    });

    Handlebars.registerHelper('toLower', function (str) {
        return str.toLowerCase();
    });

    Handlebars.registerHelper('checkbox', function (isChecked, options) {
        var checkbox = $('<input type="checkbox" />');

        if (isChecked) {
            checkbox.attr('checked', 'checked');
        }

        // this copies the values in the hash
        // over to atttributes on the checkbox
        _(_(options.hash).keys()).each(function (key) {
            var val = options.hash[key];
            checkbox.attr(key, val);
        });


        // $('<div></div>').append(checkbox).html() is a trick to return the outerHtml 
        return new Handlebars.SafeString($('<div></div>').append(checkbox).html());
    });

    Handlebars.registerHelper('yesNo', function (yesNo) {
        return (yesNo) ? 'Yes' : 'No';
    });

    //Handlebars.registerHelper('config', function (key) {
    //    return WebConfig[key];
    //});

    Handlebars.registerHelper('isNew', function (model, options) {

        // handle situation where no model was given
        if (options === undefined) {
            options = model;
            model = this;
        }

        if (_isNew(model)) {
            return options.fn(model);
        }
    });

    Handlebars.registerHelper('isNotNew', function (model, options) {

        // handle situation where no model was given
        if (options === undefined) {
            options = model;
            model = this;
        }

        if (!_isNew(model)) {
            return options.fn(model);
        }
    });


    Handlebars.registerHelper('each', function (context, options) {
        var ret = "";

        for (var i = 0, j = context.length; i < j; i++) {
            ret = ret + options.fn(context[i]);
        }

        return ret;
    });

    function _isNew(model) {
        var isNew = false;

        if (model.isNew) {
            if (typeof (model.isNew) === 'function') {
                isNew = model.isNew();
            } else {
                isNew = model.isNew;
            }
        } else {
            isNew = model.id === undefined;
        }

        return isNew;
    }

    return templateHelpers;
});