﻿define([
  'jquery'
], function ($) {

    $.fn.watermark = function () {
        this.each(function () {
            var watermark = $(this).attr("data-watermark");

            if ($(this).val() == '') {
                $(this).val(watermark).addClass("watermark");
            }

            $(this).change(function () {
                if ($(this).val() != watermark) {
                    $(this).removeClass("watermark");
                }
            });

            $(this).blur(function () {
                if ($(this).val().length == 0) {
                    $(this).val(watermark).addClass("watermark");
                }
            });

            $(this).focus(function () {
                if ($(this).val() == watermark) {
                    $(this).val('').removeClass("watermark");
                }
            });
        });

        return this;
    }

    return {};
});