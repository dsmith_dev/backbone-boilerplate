﻿require.config({
    baseUrl: 'scripts',
    paths: {

        // foundation libraries
        jquery: 'libs/jquery/jquery',
        backbone: 'libs/backbone/backbone',
        underscore: 'libs/backbone/underscore',
        marionette: 'libs/backbone/backbone.marionette',
        'backbone.wreqr': 'libs/backbone/backbone.wreqr',
        'backbone.babysitter': 'libs/backbone/backbone.babysitter',
        'backbone.validation': 'libs/backbone/backbone.validation',

        // templating plug-ins
        text: 'libs/require/text',
        tpl: 'libs/tpl/tpl',
        hbs: 'libs/handlebars/hbars',
        Handlebars: 'libs/handlebars/handlebars',

        // UI Libraries
        watermark: 'libs/jquery/jquery.watermark',
        moment: 'libs/moment/moment'
    },
    
    // shim up backbone and underscore
    shim: {
        underscore:   { exports: '_' },
        watermark:    { deps: ['jquery'] },
        backbone: { deps: ['jquery', 'underscore'], exports: 'Backbone' },
        Handlebars: { exports: 'Handlebars' }
    }
});

require([
  'backbone',
  'app'
], function (Backbone, App) {
    App.start();
    Backbone.history.start();
});
